import random
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

MEDIA_ADDR = 'http://192.168.50.42:8000/api/'

class UserExtension(models.Model):
    """自定义用户模型"""
    colour_choice = ["#4682B4", "#5F9EA0", "#008B8B", "#228B22", "#70DBDB"]
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='extension')
    login_name = models.CharField(max_length=255,verbose_name=u'登录名',unique=True)
    colour = models.CharField(max_length=7,verbose_name=u'头像颜色',default=random.choice(colour_choice),)
    phone = models.CharField(max_length=255,verbose_name=u'电话',)
    @receiver(post_save, sender=User)
    def create_user_extension(sender, instance, created, **kwargs):
        if created:
            User.objects.create(user=instance)
        else:
            instance.extension.save()
    def __str__(self):
        return self.login_name
    class Meta:
        verbose_name = u'用户扩展'
        verbose_name_plural = verbose_name



class Application(models.Model):
    name = models.CharField(max_length=255, verbose_name=u'应用名字', default="", )
    created_time = models.DateTimeField(verbose_name=u"创建时间", auto_now_add=True)
    icon = models.ImageField(blank=True, verbose_name=u"应用图标", upload_to="media/icon/", default='')
    owner = models.ForeignKey(to=User, verbose_name=u'所属人', on_delete=models.CASCADE, default="",
                              related_name='applicationowner', blank=True, null=True)

    class Meta:
        verbose_name = u'应用列表'
        verbose_name_plural = verbose_name


    def get_icon_url(self):
        '''返回应用图标的url'''
        return MEDIA_ADDR + str(self.icon)


    def __str__(self):
        return u'%s' % (self.name)


class Task(models.Model):
    """任务模型"""
    task_name = models.CharField(max_length=255, verbose_name=u'任务名',blank=True, null=True, )
    task_url = models.URLField(max_length=200, blank=True, null=True, )
    img = models.ImageField(blank=True, verbose_name=u"图片", upload_to="img", default='')
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, verbose_name=u'发送者', blank=True, null=True)
    application_task = models.ForeignKey(Application, on_delete=models.CASCADE, blank=True, null=True,
                                         related_name='applicationtask', verbose_name=u'所属应用', default='')
    created_time = models.DateTimeField(verbose_name=u"创建时间", auto_now_add=True)
    owner = models.ForeignKey(to=User, verbose_name=u'所属人', on_delete=models.CASCADE, default="",
                              related_name='taskowner', blank=True, null=True)

    class Meta:
        verbose_name = u'展示任务'
        verbose_name_plural = verbose_name

    def get_img_url(self):
        '''返回头像的url'''
        return MEDIA_ADDR + str(self.img)

    def __str__(self):
        return u'%s' % (self.task_name)


class Notice(models.Model):
    """应用通知模型"""
    SENDER_TYPE = (("system", "系统消息"), ("user", "用户消息"))
    REMIND_TYPE = (("week", "弱提醒"), ("strong", "强提醒"))
    NOTICE_TYPE = (("notice", "通知"), ("task", "任务"), ("comment", "评论"), ("apply", "好友申请"))
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, verbose_name=u'发送者')
    notice_content = models.TextField(verbose_name=u'通知内容 ',blank=True, null=True,)
    meta = models.CharField(max_length=255, default="{}", verbose_name="请求参数", null=True)
    notice_type = models.CharField(max_length=32, choices=NOTICE_TYPE, verbose_name="消息类型")
    remind_type = models.CharField(max_length=32, choices=REMIND_TYPE, verbose_name="提醒类型", default='', null=True)
    sender_type = models.CharField(max_length=32, choices=SENDER_TYPE, verbose_name="发送者类型", default='', null=True)
    application = models.ForeignKey(Application, on_delete=models.CASCADE, blank=True, null=True,
                                    related_name='notices', verbose_name=u'所属应用', default='')
    tasks = models.ManyToManyField(Task, related_name='tasknotice', verbose_name=u'所属任务', default='')
    isaccept = models.BooleanField(choices=((True, u"接受"), (False, u"拒绝"), ('', u"待定")), verbose_name=u"是否接受",
                                       default='', blank=True, null=True)
    isreaded = models.BooleanField(choices=((True, u"已读"), (False, u"未读")), default=False, verbose_name=u"是否已读",
                                       blank=True, null=True)
    istop = models.BooleanField(choices=((True, u"置顶"), (False, u"未置顶")), default=False, verbose_name=u"是否置顶")
    send_time = models.DateTimeField(verbose_name=u"发送时间", auto_now_add=True, null=True)
    owner = models.ForeignKey(to=User, verbose_name=u'所属人', on_delete=models.CASCADE, default="",
                              related_name='noticeowner', blank=True, null=True)

    class Meta:
        verbose_name = u'应用通知列表'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s,%s' % (self.user, self.notice_content)


class Comment(models.Model):
    """评论模型"""

    REMIND_TYPE = (("week", "弱提醒"), ("strong", "强提醒"))
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='commentuser', verbose_name=u'评论者',
                             blank=True, null=True)
    comment_content = models.TextField(verbose_name=u'评论内容 ')
    meta = models.CharField(max_length=255, default="{}", verbose_name="请求参数", null=True)
    remind_type = models.CharField(max_length=32, choices=REMIND_TYPE, verbose_name="提醒类型", default='', null=True)
    application = models.ForeignKey(Application, on_delete=models.CASCADE, blank=True, null=True,
                                    related_name='commentapplication', verbose_name=u'所属应用', default='')
    tasks = models.ForeignKey(Task, related_name='commenttask', on_delete=models.CASCADE, verbose_name=u'所属任务',
                              default='')
    isreaded = models.BooleanField(choices=((True, u"已读"), (False, u"未读")), default=False, verbose_name=u"是否已读",
                                       blank=True, null=True)
    istop = models.BooleanField(choices=((True, u"置顶"), (False, u"未置顶")), default=False, verbose_name=u"是否置顶")
    send_time = models.DateTimeField(verbose_name=u"发送时间", auto_now_add=True, null=True)
    owner = models.ForeignKey(to=User, verbose_name=u'接收人', on_delete=models.CASCADE, default="",
                              related_name='commentowner', blank=True, null=True)

    class Meta:
        verbose_name = u'评论模型'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s,%s' % (self.user, self.comment_content)
