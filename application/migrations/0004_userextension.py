# Generated by Django 3.1.2 on 2020-10-27 17:19

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('application', '0003_application_owner'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserExtension',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('login_name', models.CharField(max_length=255, unique=True, verbose_name='登录名')),
                ('colour', models.CharField(default='#5F9EA0', max_length=7, verbose_name='头像颜色')),
                ('phone', models.CharField(max_length=255, verbose_name='电话')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='extension', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': '用户扩展',
                'verbose_name_plural': '用户扩展',
            },
        ),
    ]
