from django.contrib.auth.models import User
from django.utils.deprecation import MiddlewareMixin


class GrantUserMiddleware(MiddlewareMixin):
    """调试阶段授予请求虚拟用户"""

    def process_request(self, request):
        log_user = User.objects.get(username='xiaosuming000')
        request.user = log_user




class DisableCSRFCheck(MiddlewareMixin):
    def process_request(selfself, request):
        setattr(request, '_dont_enforce_csrf_checks', True)
