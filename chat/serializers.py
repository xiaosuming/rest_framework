from rest_framework import serializers

from application.serializers import UserExtensionSerializer
from chat.models import GroupChat, Message, Myfriend, MessagePart, Enclosure


class MessageSerializer(serializers.ModelSerializer):
    content = serializers.SerializerMethodField(help_text="自定义属性_内容")
    # userdetail = serializers.SerializerMethodField(help_text="自定义属性_发送人")
    groupchat_name = serializers.ReadOnlyField(
        source='groupchat.groupchat_name',
        allow_null=True)

    def get_content(self, instance):
        content = []
        login_user = self.context['request'].user
        if instance.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
            content = {
                "text_content": {"content": instance.text_content, "isreaded": instance.isreaded,
                                 "istop": instance.istop,
                                 "message_type": instance.message_type, "send_time": instance.send_time},
            }
        if instance.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
            content = {
                "text_content": {
                    "user_card": {"usercard_id": instance.usercard_id, "user_name": instance.usercard.first_name,
                                  "login_name": instance.usercard.username,
                                  "ismyfriend": 0,
                                  "colour": instance.usercard.extension.colour},
                    "isreaded": instance.isreaded, "istop": instance.istop, "message_type": instance.message_type,
                    "send_time": instance.send_time},
            }

        if instance.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
            content = {
                "text_content": {"group_card": {"groupcard_id": instance.groupcard_id,
                                                "groupchat_name": instance.groupcard.groupchat_name,
                                                },
                                 "isreaded": instance.isreaded, "istop": instance.istop,
                                 "message_type": instance.message_type,
                                 "send_time": instance.send_time},
            }

        if instance.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
            content = {
                "text_content": {"task_content": {"task_id": instance.task.id, "task_name": instance.task.task_name,
                                                  "task_img": instance.task.get_img_url(),
                                                  "task_url": instance.task.task_url,
                                                  "application_name": instance.task.application_task.name,
                                                  "application_icon": instance.task.application_task.get_icon_url()},
                                 "isreaded": instance.isreaded, "istop": instance.istop,
                                 "message_type": instance.message_type,
                                 "send_time": instance.send_time},
            }
        return content

    # def get_userdetail(self, instance):
    #     login_user = self.context['request'].user
    #     userdetail = []
    #     mf = Myfriend.objects.filter(
    #         myfriendslist__id=instance.user.id, owner_id=login_user.id).first()
    #     if mf:
    #         userdetail = {
    #             "user_id": instance.user.id,
    #             'user_name': instance.user.first_name,
    #             'login_name': instance.user.username,
    #             'colour': instance.user.extension.colour,
    #             'phone': instance.user.extension.phone,
    #             # 'remark': mf.remark,
    #             "ismyfriend": 1
    #         }
    #     else:
    #         userdetail = {
    #             "user_id": instance.user.id,
    #             'user_name': instance.user.first_name,
    #             'login_name': instance.user.username,
    #             'colour': instance.user.extension.colour,
    #             'phone': instance.user.extension.phone,
    #             # 'remark': None,
    #             "ismyfriend": 0
    #         }
    #     return userdetail

    class Meta:
        model = Message
        fields = ('id', 'user', 'groupchat_name', 'isreaded', 'message_type', 'usercard', 'groupchat', 'content',
                  'text_content', 'groupcard', 'task')
        read_only_fields = ('id', 'userdetail', 'groupchat_name', 'isreaded', 'content')
        depth=1

class GroupChatSerializer(serializers.ModelSerializer):
    mes = MessageSerializer(many=True, source='messagegroupchat', read_only=True, help_text='XIAOXI',
                            allow_empty=True)  # 一对多外键，多的一方的序列化
    num = serializers.SerializerMethodField(help_text="自定义属性_未读消息")
    # members_list = serializers.SerializerMethodField(help_text="自定义属性_群成员详细信息")
    groupchat_message = serializers.SerializerMethodField(help_text="自定义属性_群最新消息")
    userlist = serializers.CharField(help_text="成员列表 ",
                                     allow_null=True, required=False, write_only=True)

    def get_num(self, instance):
        num = instance.messagegroupchat.filter(isreaded=0).count()
        return num

    # def get_members_list(self, instance):  # 这里的instance表示数据库中一个groupchat的实例
    #     login_user = self.context['request'].user  # 这是固定的写法，获取当前用户
    #     members_list = []
    #     members = instance.groupchat_members.all()
    #     for mb in members:
    #         mf = Myfriend.objects.filter(
    #             myfriendslist__id=mb.id, owner_id=login_user.id).first()
    #         if mf:
    #             members_list.append({
    #                 "user_id": mb.id,
    #                 'user_name': mb.first_name,
    #                 'login_name': mb.username,
    #                 'colour': mb.extension.colour,
    #                 'phone': mb.extension.phone,
    #                 'remark': mf.remark,
    #                 "ismyfriend": 1
    #             })
    #         else:
    #             members_list.append({
    #                 "user_id": mb.id,
    #                 'user_name': mb.first_name,
    #                 'login_name': mb.username,
    #                 'colour': mb.extension.colour,
    #                 'phone': mb.extension.phone,
    #                 'is_myfriend': 0
    #             })
    #
    #     return members_list

    def get_groupchat_message(self, instance):
        login_user = self.context['request'].user
        groupchat_message = []
        messagepart = MessagePart.objects.filter(groupchat_id=instance.id,
                                                 user_id=login_user.id).last()  # 找出清空聊天记录时最后一条消息的id
        print(messagepart)
        if messagepart is None:
            message = instance.messagegroupchat.last()
            if message:
                groupchat_message.append({
                    "text_content": message.text_content,
                    "send_time": message.send_time.strftime(
                        '%Y-%m-%d %H:%M:%S'),  # 同时展示出群消息的第一条内容
                })  # 展示详细的结果

        else:
            lastmessage_id = messagepart.lastmessage_id
            message = instance.messagegroupchat.filter(
                id__gt=lastmessage_id).last()
            if message:
                groupchat_message.append({
                    "text_content": message.text_content,
                    "send_time": message.send_time.strftime(
                        '%Y-%m-%d %H:%M:%S'),  # 同时展示出群消息的第一条内容
                })  # 展示详细的结果
        return groupchat_message

    class Meta:
        model = GroupChat
        fields = ('id', 'groupchat_name', 'is_groupchat', 'groupchat_creater', 'is_deleteed', 'num',
                  'groupchat_members', 'groupchat_message', "userlist", 'mes')
        read_only_fields = (
            'username', 'login_name', 'num', 'members_list',
            'groupchat_message')
        # depth=1




class MyfriendSerializer(serializers.ModelSerializer):
    myfrienddetail = serializers.SerializerMethodField(help_text="自定义属性_我的好友")
    is_accept = serializers.IntegerField(help_text='是否接受', required=False,
                                         write_only=True)

    def get_myfrienddetail(self, instance, ):
        login_user = self.context['request'].user
        myfrienddetail = []
        myfrienddetail.append({
            "user_id": instance.myfriendslist.id,
            'user_name': instance.myfriendslist.first_name,
            'login_name': instance.myfriendslist.username,
            'colour': instance.myfriendslist.extension.colour,
            'phone': instance.myfriendslist.extension.phone,
            'remark': instance.remark,
            "ismyfriend": 1
        })
        return myfrienddetail

    class Meta:
        model = Myfriend
        fields = ( 'myfrienddetail','myfriendslist', 'is_accept', 'remark')
        read_only_fields = ('myfrienddetail',)



class EnclosureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Enclosure
        fields = '__all__'
