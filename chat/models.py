from django.db import models
from django.contrib.auth.models import User
from application.models import Task

MEDIA_ADDR = 'http://192.168.50.42:8000/api/'


class GroupChat(models.Model):
    """群组模型"""
    groupchat_name = models.CharField(max_length=255, verbose_name=u'群名', blank=True, null=True)
    groupchat_creater = models.ForeignKey(to=User, null=True, blank=True, related_name='groupcreater',
                                          on_delete=models.SET_NULL, verbose_name=u"创建人")
    groupchat_members = models.ManyToManyField(to=User, related_name='groupmembers', verbose_name=u'群成员', blank=True)
    is_groupchat = models.BooleanField(verbose_name=u'群聊', choices=((False, '否'), (True, u'是')), default=False,
                                       blank=True, null=True)
    colour = models.CharField(max_length=7, verbose_name=u'头像颜色', null=True, )
    is_deleteed = models.BooleanField(verbose_name=u'删除', choices=((False, '否'), (True, u'是')), default=False,
                                      blank=True, null=True)
    created_time = models.DateTimeField(verbose_name=u"创建时间", auto_now_add=True)

    class Meta:
        verbose_name = u'群组'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s' % (self.groupchat_name)


class Enclosure(models.Model):
    """附件模型"""
    name = models.CharField(max_length=128, verbose_name=u'文件名', blank=True, null=True)
    size = models.IntegerField(default=0, null=True, blank=True, verbose_name=u'文件大小')
    file_type = models.CharField(max_length=128, verbose_name=u'文件类型', blank=True, null=True)
    path = models.CharField(max_length=256, verbose_name=u'存储路径', blank=True, null=True)
    md5 = models.CharField(max_length=32, verbose_name=u"文件唯一标识符", blank=True, null=True)
    colour = models.CharField(max_length=7, verbose_name=u'头像颜色', null=True)
    is_deleteed = models.BooleanField(verbose_name=u'清理', choices=((False, '否'), (True, u'是')), default=False,
                                      blank=True, null=True)
    enclosure_user = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='enclosureuser',
                                       verbose_name=u'附件发送人', blank=True, null=True)
    groupchats = models.ManyToManyField(to=GroupChat, related_name='enclosuregroupchats', verbose_name=u'附件所属群组',
                                        blank=True)
    created_time = models.DateTimeField(auto_now_add=True, verbose_name=u'创建时间')

    class Meta:
        verbose_name = u'附件'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u"%s" % (self.name,)

    def get_file_url(self):
        '''返回头像的url'''
        return MEDIA_ADDR + str(self.path)


class Message(models.Model):
    """消息模型"""
    MESSAGE_TYPE = (
        ("message_normal", "正常消息"), ("message_groupcard", "群名片"), ("message_usercard", "用户名片"), ("message_task", "任务"))
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='user_message_user', verbose_name=u'发信人',
                             blank=True, null=True)
    text_content = models.TextField(blank=True, null=True, verbose_name=u'文本内容', default='')
    message_type = models.CharField(max_length=32, choices=MESSAGE_TYPE, verbose_name="消息类型", default="message_normal",
                                    blank=True, null=True)
    isreaded = models.BooleanField(choices=((True, u"已读"), (False, u"未读")), default=False, verbose_name=u"是否已读")
    istop = models.BooleanField(choices=((True, u"置顶"), (False, u"未置顶")), default=False, verbose_name=u"是否置顶")
    groupcard = models.ForeignKey(to=GroupChat, null=True, blank=True, on_delete=models.CASCADE,
                                  verbose_name=u'名片对应的群的id')
    usercard = models.ForeignKey(to=User, null=True, blank=True, on_delete=models.CASCADE, verbose_name=u'名片对应的人的id')
    task = models.ForeignKey(to=Task, null=True, blank=True, on_delete=models.CASCADE, verbose_name=u'任务对应的任务的id')
    groupchat = models.ForeignKey(to=GroupChat, on_delete=models.CASCADE, related_name='messagegroupchat',
                                  verbose_name=u"所属群组", blank=True, null=True)
    send_time = models.DateTimeField(verbose_name=u"发送时间", auto_now_add=True)

    class Meta:
        verbose_name = u'消息'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s,%s' % (self.user, self.text_content)


class Myfriend(models.Model):
    """我的好友模型"""
    owner = models.ForeignKey(to=User, verbose_name=u'所属人', on_delete=models.CASCADE, default="", related_name='owner',
                              blank=True, null=True)
    myfriendslist = models.ForeignKey(to=User, verbose_name=u'好友详情', on_delete=models.CASCADE,
                                      related_name='myfriendslist', default="", blank=True, null=True)
    remark = models.TextField(max_length=1024, blank=True, null=True, default='', verbose_name=u'备注')

    class Meta:
        verbose_name = u'我的好友'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s' % (self.myfriendslist)


class MessagePart(models.Model):
    """清空消息模型"""
    user = models.ForeignKey(to=User, verbose_name=u'', on_delete=models.CASCADE, related_name='messagepartuser',
                             default="", blank=True, null=True)
    groupchat = models.ForeignKey(to=GroupChat, verbose_name=u'所属群聊', on_delete=models.CASCADE,
                                  related_name='messagepartgroupchat', default="", blank=True, null=True)
    lastmessage = models.ForeignKey(to=Message, verbose_name=u'最新消息', on_delete=models.CASCADE,
                                    related_name='lastmessage', default="", blank=True, null=True)

    class Meta:
        verbose_name = u'清空消息记录'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s' % (self.user)


class GroupChatPart(models.Model):
    """ 删除聊天列表模型 """
    user = models.ForeignKey(to=User, verbose_name=u'删除者', on_delete=models.CASCADE, related_name='groupchatpartuser',
                             default="", blank=True, null=True)
    groupchat = models.ForeignKey(to=GroupChat, verbose_name=u'所属群聊', on_delete=models.CASCADE,
                                  related_name='groupchatpart', default="", blank=True, null=True)
    is_deleteed = models.BooleanField(verbose_name=u'是否删除 ', choices=((False, '否'), (True, u'是')), default=False,
                                      blank=True, null=True)

    class Meta:
        verbose_name = u'删除聊天列表模型'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s' % (self.user)
